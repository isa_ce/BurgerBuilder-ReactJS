# Burger Builder
Burger Builder is an application scripted in React and deployed on Firebase, where users can order burger by choosing ingredients. You just need to simply create an account and login to order your burger.


![2020-03-31-at-14-40-19](https://user-images.githubusercontent.com/26628508/78022623-f05cfc80-735d-11ea-9293-1f61f851e97c.gif)

